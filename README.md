# myvault

A shell script for mounting a LUKS encrypted image file.

## Creating the Image File

```
$ dd if=/dev/urandom of=vaultfile.img bs=1M count=16384
$ sudo cryptsetup luksFormat vaultfile.img

WARNING!
========
This will overwrite data on vaultfile.img irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for vaultfile.img: 
Verify passphrase: 

$ sudo cryptsetup luksOpen ./vaultfile.img MyVault
$ sudo mkfs.ext4 /dev/mapper/MyVault
$ sudo cryptsetup luksClose MyVault
```