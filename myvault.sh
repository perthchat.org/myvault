#!/bin/bash

loopdevice=/dev/loop100
loopfile=/mnt/Vault/vaultfile.img

#megabytes
loopsize=256

#/dev/mapper/xxxxx when open
cryptmapper=MyVault

makefilesystem=ext4

#mountpoint of unencrypted device
mountpoint=/home/pcadmin/MyVault


#mounts encrypted loopback file
open() {
  losetup $loopdevice $loopfile
  cryptsetup open $loopdevice $cryptmapper
  mount /dev/mapper/$cryptmapper $mountpoint
}

#unmounts previously mounted loopback file
close() {
  umount $mountpoint
  cryptsetup close $cryptmapper
  losetup -d $loopdevice
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


echo loopdevice  $loopdevice
echo loopfile    $loopfile
echo loopsize    $loopsize
echo cryptmapper $cryptmapper
echo filesystem  $makefilesystem
echo mountpoint  $mountpoint
echo command     $1

$1